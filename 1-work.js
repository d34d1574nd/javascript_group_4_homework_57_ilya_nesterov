const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];


const timeSpent = tasks.filter((item) => item.category === 'Frontend');
const sumTimeSpent = timeSpent.reduce((acc, time) => {
    return acc + time.timeSpent
}, 0);
console.log('Общее количество времени, затраченное на работу над задачами из категории "Frontend": ' + sumTimeSpent + ' часов');

const bugs = tasks.filter((item) => item.type === 'bug');
const sumBugTime = bugs.reduce((acc, time) => {
    return acc + time.timeSpent
}, 0);
console.log('Общее количество времени, затраченное на работу над задачами типа "bug": ' + sumBugTime + ' часов');

const titleTask = tasks.filter((item) => item.title.includes('UI')).length;
console.log('Количество задач, имеющих в названии слово "UI": ' + titleTask + ' задачи');

const categoryFrontEnd = tasks.filter((item) => item.category === 'Frontend');
const categoryBackEnd = tasks.filter((item) => item.category === 'Backend');
const sumCategory = {Frontend: categoryFrontEnd.length, Backend: categoryBackEnd.length};
console.log('Общее количество задач каждой категории:');
console.log(sumCategory);

const dataTimeFourHours = tasks.filter((item) => item.timeSpent >= 4);
console.log('Массив задач с затраченным временем больше 4 часов:');
const newDataTimeFourHours = dataTimeFourHours.map((item) => {
    return {title: item.title, category: item.category}
});
console.log(newDataTimeFourHours);




