import React, { Component } from 'react';
import './App.css';
import Finance from "./component/Finance/Finance";
import FinanceItem from "./component/FinanceItem/FinanceItem";
import Item from "./component/Item/Item";

class App extends Component {
  state = {
    value: '',
    valueCost: '',
    list: [],
    id: 0,
    totalPrice: 0,
  };

  change = (event) => {
  this.setState({
    value: event.target.value
  });
  };

  cost = (event) => {
    this.setState({
      valueCost: event.target.value
    });
  };

  totalPriceHide = ['totalPriceHide'];
  add = ['add'];

  submit = () => {
    if (this.state.value === ' ' || this.state.valueCost === ' ') {
      return false
    } else {
      this.totalPriceHide = ['totalPrice'];
      this.add = ['added'];

      this.list = [...this.state.list];
      this.listItem = {id: this.state.id + 1, value: this.state.value, valueCost: parseInt(this.state.valueCost)};
      this.totalPriceCopy = this.state.totalPrice + this.listItem.valueCost;
      this.list.push(this.listItem);
      this.setState({value: ' ', valueCost: ' ', list: this.list, id: this.state.id + 1, totalPrice: this.totalPriceCopy});
    }
  };

  remove = (id, valueCost) => {
    const list = [...this.state.list];
    const index = list.findIndex(task => task.id === id);
    list.splice(index, 1);
    this.newTotalPriceCopy = this.state.totalPrice  - valueCost;
    this.setState({list: list, totalPrice: this.newTotalPriceCopy, id: this.state.id - 1});
    if (this.newTotalPriceCopy === 0) {
      this.totalPriceHide = ['totalPriceHide'];
      this.add = ['add'];
    }
  };

  render() {
    let list = this.state.list.map((task) => {
      return (
          <Item
              key={task.id}
              remove={() => this.remove(task.id, task.valueCost)}
              value={task.value}
              valueCost={task.valueCost}
          />
      )
    });
    return (
      <div className="App">
        <Finance
            change={event => this.change(event)}
            cost={event => this.cost(event)}
            submit={() => this.submit()}
        />
        <FinanceItem
            total={this.state.totalPrice}
            class={this.totalPriceHide}
            add={this.add}
        > {list} </FinanceItem>
      </div>
    );
  }
}

export default App;
