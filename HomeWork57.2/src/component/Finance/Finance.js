import React from 'react';
import './Finance.css';

const FinanceForm = (props) => (
    <div className="financeForm">
        <form action="#">
            <input
                type="text"
                title="Введите то, на что Вы потратили деньги сегодня"
                className="message"
                placeholder='Item name'
                onChange={props.change}
            />
            <input
                type="number"
                title="Сумма"
                className="sum"
                placeholder='Cost'
                onChange={props.cost}
            />
            <span>KGS</span>
            <button className='button' onClick={props.submit}>Add</button>
        </form>
    </div>
);

export default FinanceForm;