import React from 'react';
import './Item.css';

const Item = props => {
    return (
        <p key={props.id}>
            <span className="messageText">{props.value}</span>
            <span className="sumText">{props.valueCost} <span className="money">KGS</span></span>
            <button className="close" onClick={props.remove}><i className="fas fa-times"></i></button>
        </p>
    )
};

export default Item