import React from 'react';
import './FinanceItem.css';

const FinanceItem = props => {
    return (
        <div className="financeItem">
            {props.children}
            <span className={props.add}>Please add item name and his cost!</span>
            <div className={props.class}>
                <p className="total">Total spent:</p>
                <span>{props.total} KGS</span>
            </div>
        </div>
    )
};

export default FinanceItem;